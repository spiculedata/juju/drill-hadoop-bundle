# Apache Drill over Hadoop HDFS

## Adding Data To HDFS

### Importing SQL data

The quickest way to get started is by importing some test data into HDFS using the hdfs command line tool on the namenode.

To import a dataset run the following commands:

```
juju ssh namenode/0
wget <sample data>
hdfs hadoop fs -mkdir /user/ubuntu/sample
hdfs hadoop fs -put localdir/* /user/ubuntu/sample
```


### Querying data

To query data in Apache Drill you can navigate to the web console and run the following commands:

```
select * from `juju_hdfs_namenode`.`root`.`/sample/`
```

### Scaling

Ideally you'll run 1 Drill server on each Slave server available in your cluster, this pushes the data processing as close to the source as possible. To scale you can run
```
juju add-unit apache-drill --to <machine number>
```

You can then run 

```
juju status
```

or watch the status pane in the GUI to monitor progress.

## Find out more

http://spicule.co.uk
https://drill.apache.org/docs/file-system-storage-plugin/